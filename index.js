const Twitter = require('twitter');
const RSS     = require('rss');

// Google Cloud Function
exports.funcTwitter = async (req, res) => {
    try {
        const secret  = (req.query || {}).secret  || (req.body || {}).secret;
        const account = (req.query || {}).account || (req.body || {}).account;
        const search  = (req.query || {}).search  || (req.body || {}).search;

        if (!secret)                       throw new Error('Missing secret');
        if (secret !== process.env.SECRET) throw new Error('Bad secret');
        if (account && search)             throw new Error('Supply either account or search. Not both');
        if (!account && !search)           throw new Error('Supply either account or search');

        const rss = await (account ? userTimeline(account) : searchTweets(search));
        res.contentType('application/rss+xml; charset=utf-8')
            .status(200)
            .send(rss);

    } catch(err) {
        err = Array.isArray(err) && err.length ? err[0] : err;
        res.contentType('text/plain')
            .status(500)
            .send(err.message);
    }
};

// Takes a twitter account name, and returns the raw RSS feed
async function userTimeline(screen_name) {

    // Fetch tweets
    const tweets = await twitterClient().get('statuses/user_timeline', {
        screen_name,
        exclude_replies: true,
        include_rts:     false,
        trim_user:       true,
        count:           100,
        tweet_mode:      'extended',
    });

    // Create RSS feed
    const feed = new RSS({
        title:       `@${screen_name}`,
        description: `Twitter feed for @${screen_name}`,
    });

    // Populate RSS feed with tweets
    tweets.forEach(tweet => feed.item({
        title:       title(tweet),
        description: description(tweet),
        url:         `https://twitter.com/${screen_name}/status/${tweet.id_str}`,
        date:        tweet.created_at,
    }));

    return feed.xml();
}

async function searchTweets(q) {

    // Fetch tweets
    const tweets = await twitterClient().get('search/tweets', {
        q,
        count:      100,
        tweet_mode: 'extended',
    });

    // Create RSS feed
    const feed = new RSS({
        title:       `Twitter: ${q}`,
        description: `Twitter search feed for search term: ${q}`,
        result_type: 'recent',
    });

    // Populate RSS feed with tweets
    tweets.statuses.forEach(tweet => feed.item({
        title:       `@${tweet.user.screen_name} ${title(tweet)}`,
        description: description(tweet),
        url:         `https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`,
        date:        tweet.created_at,
    }));

    return feed.xml();
}

// Derive RSS title from tweet
function title(tweet) {
    const text = description(tweet);
    return text.length <= 64 ? text : text.substr(0, 61) + '...';
}

// Derive RSS description from tweet
function description(tweet) {
    let text = tweet.full_text;

    (tweet.entities.urls  || [])
        .forEach(url => (text = text.split(url.url).join(url.expanded_url)));

    (tweet.entities.media || [])
        .forEach(media => (text = text.split(media.url).join(media.media_url_https||media.media_url)));

    return text;
}

// Build a twitter client
function twitterClient() {
    const twitter = new Twitter({
        consumer_key:         process.env.TWITTER_consumer_key,
        consumer_secret:      process.env.TWITTER_consumer_secret,
        access_token_key:     process.env.TWITTER_access_token_key,
        access_token_secret:  process.env.TWITTER_access_token_secret,
    });
    twitterClient = () => twitter;
    return twitter;
}
